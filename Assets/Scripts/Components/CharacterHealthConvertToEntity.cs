using Unity.Entities;
using UnityEngine;
using Zenject;

public class CharacterHealthConvertToEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    private int _health = 0;
    private EntityManager _dstManager;
    private Entity _entity;
    private ISettings _settings;

    [Inject]
    public void Init(ISettings settings)
    {
        _settings = settings;
    } 

    private void Awake()
    {
        _health = _settings.HeroHealth;
    }

    public void SetCharacterHealthEntity(int hp)
    {
        _health += hp;
        _dstManager.SetComponentData(_entity, new CharacterHealthData
        {
            health = _health,
        });
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new CharacterHealthData
        {
            health = _health
        });

        _entity = entity;
        _dstManager = dstManager;
    }
}
