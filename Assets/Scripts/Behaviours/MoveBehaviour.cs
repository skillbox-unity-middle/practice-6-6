﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

public class MoveBehaviour : MonoBehaviour, IBehaviour
{
    [SerializeField] private float _minDistance = 10;
    private Vector3 _currentDestination;
    private NavMeshAgent _agent;
    private CharacterHealthConvertToEntity _character;

    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _character = FindObjectOfType<CharacterHealthConvertToEntity>();
    }

    public void Behave()
    {
        _agent.SetDestination(_currentDestination);
        gameObject.transform.LookAt(_character.transform);
    }

    public float Evaluate()
    {
        if (_character == null) return 0;
        float distance = (gameObject.transform.position - _character.transform.position).magnitude;
        if (distance < _minDistance)
        {
            if (distance > _agent.stoppingDistance)
            {
                return MoveOrStop(true);
            }
            else
            {
                return MoveOrStop(false);
            }
        }
        else
        {
            return MoveOrStop(false);
        }
    }

    private float MoveOrStop(bool isMoving)
    {
        if (isMoving)
        {
            _currentDestination = _character.transform.position;
            return float.MaxValue;
        }
        else
        {
            _currentDestination = gameObject.transform.position;
            return float.MinValue;
        }
    }
}

