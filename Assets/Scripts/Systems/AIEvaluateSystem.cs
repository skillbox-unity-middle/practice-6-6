﻿using Unity.Entities;
using UnityEngine;

public class AIEvaluateSystem : ComponentSystem
{
    private EntityQuery _evaluateQuery;

    protected override void OnCreate()
    {
        _evaluateQuery = GetEntityQuery(ComponentType.ReadOnly<AIAgent>());
    }

    protected override void OnUpdate()
    {
        Entities.With(_evaluateQuery).ForEach(
            (Entity entity, BehaviourManager manager) =>
            {
                float highScrore = float.MinValue;
                manager.currentBehaviour = null;

                foreach (var behaviour in manager.iBehaviours)
                {
                    float currentScore = behaviour.Evaluate();
                    if (currentScore > highScrore)
                    {
                        highScrore = currentScore;
                        manager.currentBehaviour = behaviour;
                    }
                }
            });
    }
}
