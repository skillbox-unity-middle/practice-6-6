﻿using Unity.Entities;
using UnityEngine;

public class CharacterMoveSystem : ComponentSystem
{
    private EntityQuery _moveQuery;
    private float _rotationSpeed = 10.0f;

    protected override void OnCreate()
    {
        _moveQuery = GetEntityQuery(ComponentType.ReadOnly<InputData>(), 
            ComponentType.ReadOnly<MoveData>(), 
            ComponentType.ReadOnly<Transform>());
    }

    protected override void OnUpdate()
    {
        Entities.With(_moveQuery).ForEach(
            (Entity entity, Transform transform, ref InputData inputData, ref MoveData moveData) =>
            {
                var pos = transform.position;
                var distance = new Vector3(inputData.move.x * moveData.speed.x, 0, inputData.move.y * moveData.speed.y);
                pos += distance;
                transform.position = pos;

                if (distance != Vector3.zero)
                {
                    Quaternion toRotation = Quaternion.LookRotation(distance, Vector3.up);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, _rotationSpeed);
                }

            });
    }
}
